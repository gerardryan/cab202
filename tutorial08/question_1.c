#include <stdbool.h>
#include "graphics.h"
#include "cpu_speed.h"
#include "sprite.h"

#define CHAR_W 5
#define CHAR_H 8
#define LINE_SPACE 2

int main() {
	set_clock_speed(CPU_8MHz);

	lcd_init(LCD_DEFAULT_CONTRAST);

	clear_screen();

	draw_line(LCD_X - 1, 0, LCD_X - 1, LCD_Y);

	draw_string((LCD_X - (CHAR_W * 6)) / 2, ((LCD_Y - ((CHAR_H * 2) + LINE_SPACE)) / 2), "Gerard");
	draw_string((LCD_X - (CHAR_W * 8)) / 2, ((LCD_Y - ((CHAR_H * 2) + LINE_SPACE)) / 2 + CHAR_H + LINE_SPACE), "n8608687");

	Sprite box;
	init_sprite(&box, 0, 0, 8, 8, (unsigned char[]) {
		0xff, // 1111 1111
		0x81, // 1000 0001
		0x81, // 1000 0001
		0x81, // 1000 0001
		0x81, // 1000 0001
		0x81, // 1000 0001
		0x81, // 1000 0001
		0xff  // 1111 1111
	});
	draw_sprite(&box);

	show_screen();

	return 0;
}