#ifndef MAIN_H
#define MAIN_H

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <curses.h>

// zdk
#include "cab202_timers.h"

#include "platform.h"
#include "player.h"
#include "box.h"
#include "Boss.h"

#define SEED (unsigned int)gameStartTime

#define SCREEN_WIDTH 70
#define SCREEN_HEIGHT 55
#define REFRESH_RATE 15 // Hz
#define JOYSTICK_TIMEOUT 150 // ms

#define BOUNDARY_HEIGHT 3

#define SLOW_GAME_SPEED 0.25
#define NORMAL_GAME_SPEED 1
#define FAST_GAME_SPEED 4
#define MAX_LEVEL 3

extern double gameStartTime;
extern double deltaTime; // time to complete the main loop

// game elements
extern PlatformList platforms;
extern Platform* initalPlatform; // used to see when screen is full
extern Player player;
extern float currGameSpeed;
extern int level;
extern bool gameRestarting;

// timers
extern timer_id screenRefresh;
extern timer_id platformSpawn;
extern timer_id bossSpawn;

// Used to setup game state
void onStartup();

// cleans up any memory for a clean exit
void onCleanup();

// reset the game state to a new life state or just a new game 
void reset(bool newLife);

// Used to update game state
void onUpdate();

// Checks and dispatches keyboard inputs
void checkInput();

// Updates the game speed gradually
void updateGameSpeed();

// used to draw game state
void onDraw();

// Draw the standard user interface or HUD with values
void drawUI();

// Draws the boundary at the top of the window with lives remaining and play time
void drawHeadderBoundary();

// Draws the boundary at the bottom of the window with level, score and game speed
void drawFooterBoundary();

// Draw the game state
void drawGame();

#endif // !main.h 