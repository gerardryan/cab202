#include "box.h"

// Returns weather box 1 and box 2 would/have collided
int boxCollided(Box b1, Box b2) {
	int b1Left = round(b1.x);
	int b1Right = b1Left + round(b1.width - 1);
	int b1Top = round(b1.y);
	int b1Bottom = b1Top + round(b1.height - 1);

	int b2Left = round(b2.x);
	int b2Right = b2Left + round(b2.width - 1);
	int b2Top = round(b2.y);
	int b2Bottom = b2Top + round(b2.height - 1);

	if (b1Left <= b2Right && b1Right >= b2Left && b1Top <= b2Bottom && b1Bottom >= b2Top) { // if collision
		int collisions = 0;
		collisions ^= (-(b1Bottom >= b2Top && b1Bottom <= b2Bottom) ^ collisions) & TOP_COLLISION_BIT_MASK; // b1 collided with b2's top
		collisions ^= (-(b1Top <= b2Bottom && b1Top >= b2Top) ^ collisions) & BOTTOM_COLLISION_BIT_MASK; // b1 collided with b2's bottom
		collisions ^= (-(!(collisions & (TOP_COLLISION_BIT_MASK | BOTTOM_COLLISION_BIT_MASK))) ^ collisions) & HEIGHT_ENCLOSING_COLLISION_BIT_MASK; // b1 is enclosing the height of b2

		collisions ^= (-(b1Right >= b2Left && b1Right <= b2Right) ^ collisions) & LEFT_COLLISION_BIT_MASK; // b1 collided with b2's left
		collisions ^= (-(b1Left <= b2Right && b1Left >= b2Left) ^ collisions) & RIGHT_COLLISION_BIT_MASK; // b1 collided with b2's right
		collisions ^= (-(!(collisions & (LEFT_COLLISION_BIT_MASK | RIGHT_COLLISION_BIT_MASK))) ^ collisions) & WIDTH_ECCLOSING_COLLISION_BIT_MASK; // b1 is enclosing the width of b2
		return collisions;
	}
	return 0;
}

// Draws the given box on the screen with the given width and height with the given vertical and horizontal characters 
void drawBox(Box box, char vertical, char horizontal) {
	draw_line(box.x, box.y, box.x + (box.width - 1), box.y, horizontal); // top
	draw_line(box.x, box.y + 1, box.x, box.y + (box.height - 2), vertical); // left
	draw_line(box.x + (box.width - 1), box.y + 1, box.x + (box.width - 1), box.y + (box.height - 2), vertical); // right
	draw_line(box.x, box.y + (box.height - 1), box.x + (box.width - 1), box.y + (box.height - 1), horizontal); // bottom
}

// Draws the given box on the screen with the given width and height with the given vertical, horizontal and corner characters
void drawBoxWithCorners(Box box, char vertical, char horizontal, char corner) {

	// draw lines
	draw_line(box.x + 1, box.y, box.x + (box.width - 2), box.y, horizontal); // top
	draw_line(box.x, box.y + 1, box.x, box.y + (box.height - 2), vertical); // left
	draw_line(box.x + (box.width - 1), box.y + 1, box.x + (box.width - 1), box.y + (box.height - 2), vertical); // right
	draw_line(box.x + 1, box.y + (box.height - 1), box.x + (box.width - 2), box.y + (box.height - 1), horizontal); // bottom

																					   // draw corners
	draw_char(box.x, box.y, corner); // top left
	draw_char(box.x + (box.width - 1), box.y, corner); // top right
	draw_char(box.x, box.y + (box.height - 1), corner); // bottom left
	draw_char(box.x + (box.width - 1), box.y + (box.height - 1), corner); // bottom right
}