#include "main.h"

bool quit = false;
bool gameRestarting = false;

// speed
bool speedChanging = false;
float nextGameSpeed = 1;
float lastGameSpeed = 1;
float currGameSpeed = 1;

// times
double gameStartTime;
double deltaTime = 0; // time to complete the main loop

// game elements
PlatformList platforms;
Platform* initalPlatform; // used to see when screen is full
Player player;
int level;

// timers
timer_id screenRefresh;
timer_id resetJoystickX;
timer_id resetJoystickY;

timer_id platformSpawn;
timer_id bossSpawn;

int main(int argc, const char* argv[]) {
	//auto_save_screen = true;

	// screen and application setup
	setup_screen();
	override_screen_size(SCREEN_WIDTH, SCREEN_HEIGHT);
	gameStartTime = get_current_time();
	srand(SEED);

	// setup timers
	screenRefresh = create_timer(1000 / REFRESH_RATE);
	platformSpawn = create_timer(PLATFORM_SPAWN_TIME);
	resetJoystickX = create_timer(JOYSTICK_TIMEOUT);
	resetJoystickY = create_timer(JOYSTICK_TIMEOUT);
	bossSpawn = create_timer(rand() % BOSS_SPAWN_TIME_RANGE);

	onStartup();
	
	double loopStartTime;
	while (!quit) {
		loopStartTime = get_current_time();

		onUpdate();
		onDraw();

		deltaTime = get_current_time() - loopStartTime;
	}

	onCleanup();

	destroy_timer(screenRefresh);
	destroy_timer(platformSpawn);
	destroy_timer(resetJoystickX);
	destroy_timer(resetJoystickY);
	destroy_timer(bossSpawn);
	cleanup_screen();

	return EXIT_SUCCESS;
}

// Used to setup game state
void onStartup() {
	level = 3;
	setupInitalPlatform();
	setupPlayer(0, START_PLAYER_LIVES);
}

// cleans up any memory for a clean exit
void onCleanup() {
	
	// remove platforms
	if (initalPlatform != NULL) {
		initalPlatform = NULL;
	}
	while (platforms.head != NULL) {
		deletePlatform(removePlatform(&platforms, platforms.head, NULL));
	}

	// remove player
	deletePlayer();

	// remove boss
	deleteBoss();
}

// reset the game state to a new life state or just a new game 
void reset(bool newLife) {
	setupInitalPlatform();
	deleteBoss();

	if (newLife) {
		setupPlayer(player.score, player.lives - 1);
	} else {
		setupPlayer(0, START_PLAYER_LIVES);
		gameStartTime = get_current_time();
	}
}

// Used to update game state
void onUpdate() {
	checkInput();
	updateBoss();
	updatePlatforms();
	updatePlayer();
	updateGameSpeed();
}

// Checks and dispatches keyboard inputs
void checkInput() {
	int input = get_char();

	// quit
	if (input == 'q') {
		quit = true;
	}

	// level
	if (input == 'l') {

		// decrement level
		level--;
		level = (level < 1) ? MAX_LEVEL : level;

		// revert game speed to normal
		if (level < 3) {
			deleteBoss();
			if (currGameSpeed != NORMAL_GAME_SPEED) {
				nextGameSpeed = NORMAL_GAME_SPEED;
				speedChanging = true;
			}
		}
	}

	// change game speed
	if (level == 3 && !speedChanging) {
		if (input == '1' && currGameSpeed != SLOW_GAME_SPEED) {
			nextGameSpeed = SLOW_GAME_SPEED;
			speedChanging = true;
		} else if (input == '2' && currGameSpeed != NORMAL_GAME_SPEED) {
			nextGameSpeed = NORMAL_GAME_SPEED;
			speedChanging = true;
		} else if (input == '3' && currGameSpeed != FAST_GAME_SPEED) {
			nextGameSpeed = FAST_GAME_SPEED;
			speedChanging = true;
		}

		if (speedChanging) {
			lastGameSpeed = currGameSpeed;
		}
	}

	// restarting
	if (gameRestarting) {
		if (input == 'r') {
			reset(false);
			gameRestarting = false;
		}
	} else {

		// direction
		if ((input == KEY_UP) ^ (input == KEY_DOWN)) { // if either key up or key down
			timer_reset(resetJoystickY);
			if ((input == KEY_UP)) {
				player.inDy = -1;
			} else {
				player.inDy = 1;
			}
		} else if (timer_expired(resetJoystickY)) {
			player.inDy = 0;
		}

		if ((input == KEY_LEFT) ^ (input == KEY_RIGHT)) { // if either key left or key down
			timer_reset(resetJoystickX);
			if (input == KEY_LEFT) {
				player.inDx = -1;
			} else {
				player.inDx = 1;
			}
		} else if (timer_expired(resetJoystickX)) {
			player.inDx = 0;
		}
	}
}

// Updates the game speed gradually
#define GAME_SPEED_THRESHOLD 0.1f
void updateGameSpeed() {
	if (currGameSpeed < nextGameSpeed) {
		if (currGameSpeed > nextGameSpeed - GAME_SPEED_THRESHOLD) {
			currGameSpeed = nextGameSpeed;
			speedChanging = false;
		} else {
			currGameSpeed += (1 * deltaTime);
		}
	} if (currGameSpeed > nextGameSpeed) {
		if (currGameSpeed < nextGameSpeed + GAME_SPEED_THRESHOLD) {
			currGameSpeed = nextGameSpeed;
			speedChanging = false;
		} else {
			currGameSpeed -= (1 * deltaTime);
		}
	}
	platformSpawn->milliseconds = PLATFORM_SPAWN_TIME / currGameSpeed;
}

// used to draw game state
void onDraw() {
	if (timer_expired(screenRefresh)) {
		clear_screen();

		drawGame();
		drawUI();

		show_screen();
	}
}

// Draw the standard user interface or HUD with values
void drawUI() {
	drawHeadderBoundary();
	drawFooterBoundary();

	if (gameRestarting) {
		draw_string((screen_width() - 66) / 2, screen_height() / 2, "The game can be restarted by pressing 'r' or quit with the 'q' key");
		drawBox((Box) { ((screen_width() - 66) / 2) - 1, (screen_height() / 2) - 1, 68, 3 }, ' ', ' ');
	}
}

// Draws the boundary at the top of the window with lives remaining and play time
void drawHeadderBoundary() {
	char verticalChar = '|';
	char horizontalChar = '-';
	char cornerChar = '+';

	// clear area for headder
	for (int y = 0; y < BOUNDARY_HEIGHT; y++) {
		for (int x = 0; x < screen_width(); x++) {
			draw_char(x, y, ' ');
		}
	}

	// outline
	drawBoxWithCorners((Box){ 0, 0, screen_width(), BOUNDARY_HEIGHT }, verticalChar, horizontalChar, cornerChar);

	// time
	double currentTime = get_current_time();
	unsigned int minutes = (unsigned int)(currentTime - gameStartTime) / 60;
	if (minutes >= 99) { // clamp minutes
		minutes = 99;
	}
	unsigned int seconds = (unsigned int)(currentTime - gameStartTime) % 60;
	draw_formatted((screen_width() - 1) - 11, (BOUNDARY_HEIGHT - 1) / 2, "Time: %02u:%02u", minutes, seconds);

	// lives
	draw_formatted(1, (BOUNDARY_HEIGHT - 1) / 2, "Lives Remaining: %i", player.lives);
}

// Draws the boundary at the bottom of the window with level, score and game speed
void drawFooterBoundary() {
	char verticalChar = '|';
	char horizontalChar = '-';
	char cornerChar = '+';

	// clear area for footer
	for (int y = 0; y < BOUNDARY_HEIGHT; y++) {
		for (int x = 0; x < screen_width(); x++) {
			draw_char(x, screen_height() - BOUNDARY_HEIGHT + y, ' ');
		}
	}

	// outline
	drawBoxWithCorners((Box){0, screen_height() - BOUNDARY_HEIGHT, screen_width(), BOUNDARY_HEIGHT}, verticalChar, horizontalChar, cornerChar);

	// level
	draw_formatted(1, (screen_height() - 1) - ((BOUNDARY_HEIGHT - 1) / 2), "Level: %i ", level);

	// score
	draw_formatted(10, (screen_height() - 1) - ((BOUNDARY_HEIGHT - 1) / 2), "Score: %i", player.score);

	// speed
	if (level == 3) {
		char* lastGameSpeedStr = "";
		char* change = "";
		char* nextGameSpeedStr = "";
		if (speedChanging) {
			change = " -> ";

			if (lastGameSpeed == SLOW_GAME_SPEED) {
				lastGameSpeedStr = "SLOW";
			} else if (lastGameSpeed == NORMAL_GAME_SPEED) {
				lastGameSpeedStr = "NORMAL";
			} else if (lastGameSpeed == FAST_GAME_SPEED) {
				lastGameSpeedStr = "FAST";
			}

			if (nextGameSpeed == SLOW_GAME_SPEED) {
				nextGameSpeedStr = "SLOW";
			} else if (nextGameSpeed == NORMAL_GAME_SPEED) {
				nextGameSpeedStr = "NORMAL";
			} else if (nextGameSpeed == FAST_GAME_SPEED) {
				nextGameSpeedStr = "FAST";
			}
		} else {
			if (currGameSpeed == SLOW_GAME_SPEED) {
				nextGameSpeedStr = "SLOW";
			} else if (currGameSpeed == NORMAL_GAME_SPEED) {
				nextGameSpeedStr = "NORMAL";
			} else if (currGameSpeed == FAST_GAME_SPEED) {
				nextGameSpeedStr = "FAST";
			}
		}
		draw_formatted((screen_width() - 1) - (strlen(lastGameSpeedStr) + strlen(change) + strlen(nextGameSpeedStr)), (screen_height() - 1) - ((BOUNDARY_HEIGHT - 1) / 2), "%s%s%s", lastGameSpeedStr, change, nextGameSpeedStr);
	}
}

// Draw the game state
void drawGame() {
	drawPlatforms();
	sprite_draw(player.sprite);
	drawBoss();
}