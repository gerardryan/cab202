#ifndef BOX_H
#define BOX_H

typedef struct Box Box;

#include <math.h>

// zdk
#include "cab202_graphics.h"

#define NO_COLLISION_BIT_MASK 0x0                    // 000000

#define TOP_COLLISION_BIT_MASK (1 << 0)              // 000001
#define BOTTOM_COLLISION_BIT_MASK (1 << 1)           // 000010
#define LEFT_COLLISION_BIT_MASK (1 << 2)             // 000100
#define RIGHT_COLLISION_BIT_MASK (1 << 3)            // 001000

#define HEIGHT_ENCLOSED_COLLISION_BIT_MASK 0x3       // 000011
#define WIDTH_ENCLOSED_COLLISION_BIT_MASK 0xC        // 001100
#define ENCLOSED_COLLISION_BIT_MASK 0xF              // 001111

#define HEIGHT_ENCLOSING_COLLISION_BIT_MASK (1 << 4) // 010000
#define WIDTH_ECCLOSING_COLLISION_BIT_MASK (1 << 5)  // 100000
#define ECCLOSING_COLLISION_BIT_MASK 0x30            // 110000

struct Box {
	double x, y, width, height;
};

// Returns weather box 1 and box 2 would/have collided
int boxCollided(Box b1, Box b2);

// Draws the given box on the screen with the given width and height with the given vertical and horizontal characters 
void drawBox(Box box, char vertical, char horizontal);

// Draws the given box on the screen with the given width and height with the given vertical, horizontal and corner characters
void drawBoxWithCorners(Box box, char vertical, char horizontal, char corner);

#endif // !BOX_H