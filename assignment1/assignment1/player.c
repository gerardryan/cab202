#include "player.h"

// check collisions and sets player state
void checkCollision() {
	int currPlatformCollisions = 0;
	player.collisions = 0;
	Box playerCollider = {player.sprite->x, player.sprite->y, player.sprite->width, player.sprite->height + PLATFORM_PROXIMITY_LEEWAY}; // increase a bit to improve platform collision

	for (Platform* platform = platforms.head; platform != NULL; platform = platform->next) {
		Box platformCollider = {platform->sprite->x, platform->sprite->y, platform->sprite->width, platform->sprite->height};

		if ((currPlatformCollisions = boxCollided(playerCollider, platformCollider))) { // check collided with platform
			player.collisions |= currPlatformCollisions; // acumulate collisions

			// if above or too big
			if (currPlatformCollisions & TOP_COLLISION_BIT_MASK) {
				if (player.sprite->y + player.sprite->height > platform->sprite->y) {
					player.sprite->y = platform->sprite->y - player.sprite->height; // move to above the platform
				}				
				if (level == 1 || player.sprite->dy > platform->sprite->dy * currGameSpeed) {
					player.sprite->dy = platform->sprite->dy * currGameSpeed; // null the downwards velocity 
					player.sprite->y = platform->sprite->y - player.sprite->height; // move to above the platform because collision box will prematurely stop velocity
				}

				// landed on top of a safe platform
				if (!platform->deadly && (platform != player.lastCollision && platform != player.secondLastCollision)) {
					player.score++;
				}
				player.secondLastCollision = player.lastCollision;
				player.lastCollision = platform;

			// if below, left, right or inside
			} else if (currPlatformCollisions & ENCLOSED_COLLISION_BIT_MASK) {
				
				// if below
				if (currPlatformCollisions & BOTTOM_COLLISION_BIT_MASK) {
					if (player.sprite->y < platform->sprite->y + platform->sprite->height) {
						player.sprite->y = platform->sprite->y + player.sprite->height; // move to below the platform
					}
					if (player.sprite->dy < platform->sprite->dy * currGameSpeed) {
						player.sprite->dy = platform->sprite->dy * currGameSpeed; // null the upwards velocity 
					}
				}

				// colliding with the left
				if ((currPlatformCollisions & LEFT_COLLISION_BIT_MASK) && !(currPlatformCollisions & RIGHT_COLLISION_BIT_MASK)) {
					if (player.sprite->x + player.sprite->width > platform->sprite->x) {
						player.sprite->x = platform->sprite->x - player.sprite->width; // move to the left of the platform 
					}
					if (level > 1 && player.sprite->dx > platform->sprite->dx * currGameSpeed) {
						player.sprite->dx = platform->sprite->dx * currGameSpeed; // null the rightwards velocity 
					}

				// colliding with the right
				} else if ((currPlatformCollisions & RIGHT_COLLISION_BIT_MASK) && !(currPlatformCollisions & LEFT_COLLISION_BIT_MASK)) {
					if (player.sprite->x < platform->sprite->x + platform->sprite->width) {
						player.sprite->x = platform->sprite->x + platform->sprite->width; // move to the right of the platform 
					}
					if (level > 1 && player.sprite->dx < platform->sprite->dx * currGameSpeed) {
						player.sprite->dx = platform->sprite->dx * currGameSpeed; // null the leftwards velocity
					}

				// in the center
				} else if (currPlatformCollisions & WIDTH_ENCLOSED_COLLISION_BIT_MASK) {

					// on the far left
					if ((int)round(player.sprite->x) == (int)round(platform->sprite->x)) { // rounded to an int to look better
						if (player.sprite->x + player.sprite->width > platform->sprite->x) {
							player.sprite->x = platform->sprite->x - player.sprite->width; // move to the left of the platform 
						}
						if (level > 1 && player.sprite->dx > platform->sprite->dx * currGameSpeed) {
							player.sprite->dx = platform->sprite->dx * currGameSpeed; // null the rightwards velocity 
						}

					// on the far right
					} else if ((int)round(player.sprite->x) + player.sprite->width == (int)round(platform->sprite->x) + platform->sprite->width) { // rounded to an int to look better
						if (player.sprite->x < platform->sprite->x + platform->sprite->width) {
							player.sprite->x = platform->sprite->x + platform->sprite->width; // move to the right of the platform 
						}
						if (level > 1 && player.sprite->dx < platform->sprite->dx * currGameSpeed) {
							player.sprite->dx = platform->sprite->dx * currGameSpeed; // null the leftwards velocity
						}
					}
				} 
			}

			if (platform->deadly) {

				// only if it also looks like that we collided with the bottom of a deadly platform reset player
				if (currPlatformCollisions & BOTTOM_COLLISION_BIT_MASK && round(player.sprite->y) < round(platform->sprite->y) + platform->sprite->height) {
					player.reset = true;
				} else {
					player.reset = true;
				}
			}
		}
	}

	if (boss.active) {
		Box bossCollider = {boss.sprite->x, boss.sprite->y, boss.sprite->width, boss.sprite->height};

		if (boxCollided(playerCollider, bossCollider)) {
			int bossCenterX = boss.sprite->x + boss.spriteRadius;
			int bossCenterY = boss.sprite->y + boss.spriteRadius;

			// lazy mans collision because player is so small
			for (int x = 0; x < player.sprite->width; x++) {
				for (int y = 0; y < player.sprite->height; y++) {

					// this player point relitive to the bosses center
					int centerToPlayerX = bossCenterX - (round(player.sprite->x) + x);
					int centerToPlayerY = bossCenterY - (round(player.sprite->y) + y);

					if (round(sqrt(pow(centerToPlayerX, 2) + pow(centerToPlayerY, 2))) <= boss.spriteRadius) {
						player.reset = true;
					}
				}
			}
		}
	}
}

// Sets up the mostly default player with some values required
void setupPlayer(int score, int lives) {
	player.collisions = 0;
	player.inDx = 0;
	player.inDy = 0;
	player.score = score;
	player.lives = lives;
	player.reset = false;
	player.lastCollision = platforms.head;
	player.secondLastCollision = platforms.head;

	if (player.sprite != NULL) {
		sprite_destroy(player.sprite);
	}

	player.sprite = sprite_create((screen_width() - ((PLATFORM_WIDTH / 2) + (PLAYER_WIDTH / 2))) / 2,
									  screen_height() - (PLAYER_HEIGHT + PLATFORM_HEIGHT + BOUNDARY_HEIGHT),
									  PLAYER_WIDTH, PLAYER_HEIGHT,
									  PLAYER_BITMAP);
}

// frees memory allocations for the player
void deletePlayer() {
	sprite_destroy(player.sprite);
}

// Update player behavour such as movement and dying
void updatePlayer() {
	if (!gameRestarting) {
		checkCollision();
		movePlayer();

		if (player.reset) {
			if (player.lives > 1) {
				reset(true);
			} else {
				gameRestarting = true; // Show restarting UI and prevent game from progressing
			}
			player.reset = false;
		}
	}
}

// moves the player according to gravity and controlls
void movePlayer() {

	// move according to input or gravity
	if (player.collisions & TOP_COLLISION_BIT_MASK) {

		// movement
		if (player.inDx) {
			if (level < 2) {
				player.sprite->dx = (PLAYER_MOVEMENT_SPEED * player.inDx);
			} else {

				// set imeadiate speed to look responsive
				if (fabs(player.sprite->dx) < PLAYER_MOVEMENT_SPEED) {
					player.sprite->dx = (PLAYER_MOVEMENT_SPEED * player.inDx);
				}
				player.sprite->dx += ((PLAYER_MOVEMENT_ACCELERATION * player.inDx) * deltaTime);
			}
		} else { // "friction"
			if (round(player.sprite->dx) < 0) {
				player.sprite->dx += (PLAYER_MOVEMENT_ACCELERATION * deltaTime);
			} else if (round(player.sprite->dx) > 0) {
				player.sprite->dx -= (PLAYER_MOVEMENT_ACCELERATION * deltaTime);
			} else {
				player.sprite->dx = 0;
			}
		}

		if (level > 1) {
			if (player.inDy == -1) {
				player.sprite->y -= PLATFORM_PROXIMITY_LEEWAY; // because player sticks to the platform in this range move out of it
				player.sprite->dy = PLAYER_JUMP;
			} else if (player.inDy == 1) {
				player.sprite->dx = 0;
			}
		}

	} else if (level > 1) {
		player.sprite->dy += (PLAYER_GRAVITY * deltaTime);
	} else {
		player.sprite->dx = 0;
		player.sprite->dy = 2;
	}

	// move according to side barriers
	if (player.sprite->x > screen_width() - 1) {
		player.sprite->dx = 0;
		player.sprite->x = screen_width() - 1;
	} else if (player.sprite->x < 0) {
		player.sprite->dx = 0;
		player.sprite->x = 0;
	}

	// reset if going off screen
	if (player.sprite->y + player.sprite->height > screen_height() - BOUNDARY_HEIGHT || player.sprite->y < BOUNDARY_HEIGHT) {
		player.reset = true;
	}

	// move after calculating forces
	sprite_move(player.sprite, (player.sprite->dx * deltaTime), (player.sprite->dy * deltaTime));
}