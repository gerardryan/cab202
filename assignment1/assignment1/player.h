#ifndef PLAYER_H
#define PLAYER_H

typedef struct Player Player;

#include <stdlib.h>

// zdk
#include "cab202_sprites.h"

#include "main.h"
#include "box.h"
#include "platform.h"

#define PLAYER_HEIGHT 3
#define PLAYER_WIDTH 1
#define PLAYER_BITMAP "o-^"

#define PLAYER_GRAVITY (9.80665 * 0.66666) // char/s^2
#define PLAYER_MOVEMENT_ACCELERATION (5.5 * 0.66666) // char/s^2
#define PLAYER_MOVEMENT_SPEED 2
#define PLAYER_JUMP -PLAYER_GRAVITY + (platforms.head->sprite->dy * currGameSpeed)  // char/s^2
#define PLATFORM_PROXIMITY_LEEWAY 0.5

#define START_PLAYER_LIVES 3

struct Player {
	sprite_id sprite;
	short inDx, inDy; // control input
	int collisions;
	int score, lives;
	bool reset;
	Platform* lastCollision;
	Platform* secondLastCollision;
};

// check collisions and sets player state
void checkCollision();

// Sets up the mostly default player with some values required
void setupPlayer(int score, int lives);

// frees memory allocations for the player
void deletePlayer();

// Update player behavour such as movement and dying
void updatePlayer();

// moves the player according to gravity and controlls
void movePlayer();

#endif // !PLAYER_H