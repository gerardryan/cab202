#include "Boss.h"

Boss boss;

// sets up the a boss with random attributes readdy for behavour updating
void setupBoss() {
	if (boss.active) {
		deleteBoss();
	}

	// setup 
	boss.active = true;
	boss.loopRadius = BOSS_LOOP_RADIUS;
	boss.Looping = false;
	boss.loopStartY = screen_height();
	boss.apexReached = false;
	int bossSize = rand() % (BOSS_SIZE_MAX - BOSS_SIZE_MIN + 1) + BOSS_SIZE_MIN; // + 1 to account for 0
	boss.spriteRadius = round((bossSize - 1) / 2);
	char* bitmap = malloc(sizeof(char) * (bossSize * bossSize));
	boss.sprite = sprite_create(-bossSize, rand() % (screen_height() - (BOUNDARY_HEIGHT * 2) - (boss.loopRadius * 2) - bossSize) + (boss.loopRadius * 2) + BOUNDARY_HEIGHT, bossSize, bossSize, bitmap);
	
	// caclulate destination and destination velocity
	int destinationY = 0;
	do {
		destinationY = rand() % (screen_height() - (BOUNDARY_HEIGHT * 2) - boss.loopRadius - bossSize) + boss.loopRadius + BOUNDARY_HEIGHT;
	} while (boss.sprite->y == destinationY);

	// to destination
	boss.destinationDx = screen_width() - boss.sprite->x;
	boss.destinationDy = destinationY - boss.sprite->y;

	// distance to destination
	double destinationMag = sqrt(pow(boss.destinationDx, 2) + pow(boss.destinationDy, 2));

	// normalized direction
	boss.destinationDx /= destinationMag;
	boss.destinationDy /= destinationMag;

	// set as inital velocity
	boss.sprite->dx = boss.destinationDx * BOSS_TRAVEL_SPEED;
	boss.sprite->dy = boss.destinationDy * BOSS_TRAVEL_SPEED;

	drawBitmap();
}

// frees memory allocations for the boss
void deleteBoss() {
	boss.active = false;
	boss.loopRadius = 0;
	boss.Looping = false;
	boss.loopStartY = 0;
	boss.spriteRadius = 0;
	boss.apexReached = false;
	boss.destinationDx = 0;
	boss.destinationDy = 0;
	if (boss.sprite != NULL) {
		if (boss.sprite->bitmap != NULL) {
			free(boss.sprite->bitmap);
			boss.sprite->bitmap = NULL;
		}
		sprite_destroy(boss.sprite);
		boss.sprite = NULL;
	}
}

// updates the bosses behavour
void updateBoss() {
	if (!gameRestarting) {
		if (level < 3 || boss.active) {
			timer_reset(bossSpawn);
		} else if (level == 3 && timer_expired(bossSpawn)) { // spawn
			setupBoss();
			bossSpawn->milliseconds = rand() % BOSS_SPAWN_TIME_RANGE;
		}

		moveBoss();
		drawBitmap();
	} else {
		timer_reset(bossSpawn); // prvent boss spawning as soon as a L3 restarts
	}
}

// moves the boss in a curly wirly mannor
void moveBoss() {
	if (boss.active) {

		// if boss is in the center of the screen and loop hasn't started
		if (round(boss.sprite->x) == screen_width() / 2 && boss.loopStartY == screen_height()) {
			boss.Looping = true;
			boss.loopStartY = round(boss.sprite->y);
		}

		if (boss.Looping) {

			// get centripetal acceleration magnitude
			double CentripetalAccelerationMag = pow(BOSS_TRAVEL_SPEED, 2) / boss.loopRadius;

			// get relitive center seeking vector
			double centerSeakingX = (screen_width() / 2) - boss.sprite->x;
			double centerSeakingY = (boss.loopStartY - boss.loopRadius) - boss.sprite->y;

			double centerSeakingMag = sqrt(pow(centerSeakingX, 2) + pow(centerSeakingY, 2));

			// normalize
			centerSeakingX /= centerSeakingMag;
			centerSeakingY /= centerSeakingMag;

			// apply acceleration 
			boss.sprite->dx += ((centerSeakingX * CentripetalAccelerationMag) * deltaTime);
			boss.sprite->dy += ((centerSeakingY * CentripetalAccelerationMag) * deltaTime);

			if (round(boss.sprite->y) == (boss.loopStartY - (boss.loopRadius * 2))) { // approx apex reached
				boss.apexReached = true;
			}

			if (boss.apexReached) {
				if (round(boss.sprite->y) == boss.loopStartY) { // returned to starting position
					boss.Looping = false;
					boss.sprite->dx = boss.destinationDx * BOSS_TRAVEL_SPEED;
					boss.sprite->dy = boss.destinationDy * BOSS_TRAVEL_SPEED;
				}
			}
		}

		sprite_move(boss.sprite, boss.sprite->dx * deltaTime, boss.sprite->dy * deltaTime);

		if (boss.sprite->x > (screen_width() - 1)) {
			deleteBoss();
		}
	}
}

// Draws the a circular sprite to the boss bitmap with an arrow specifying the direction of travel
void drawBitmap() {
	if (boss.active) {
		int offset = 0;
		for (int x = 0; x < boss.sprite->width; x++) {
			for (int y = 0; y < boss.sprite->height; y++) {
				if (inCircle(boss.spriteRadius, x, y, boss.sprite->width, boss.sprite->height)) {

					// movement horizontally is the largest
					if (fabs(boss.sprite->dx) > fabs(boss.sprite->dy)) {

						// moving right
						if (boss.sprite->dx > 0.0) {
							boss.sprite->bitmap[offset++] = '<';

						// moving left
						} else {
							boss.sprite->bitmap[offset++] = '>';
						}

					// movement vertically is the largest
					} else if (fabs(boss.sprite->dy) > fabs(boss.sprite->dx)) {

						// moving down
						if (boss.sprite->dy > 0.0) {
							boss.sprite->bitmap[offset++] = 'V';

						// moving up
						} else {
							boss.sprite->bitmap[offset++] = '^';
						}

					// moving diagonally 
					} else {

						// moving down
						if (boss.sprite->dy > 0.0) {
							boss.sprite->bitmap[offset++] = 'V';

						// moving up
						} else {
							boss.sprite->bitmap[offset++] = '^';
						}
					}
				} else {
					boss.sprite->bitmap[offset++] = ' ';
				}
			}
		}
	}
}

// draws the boss if it is active
void drawBoss() {
	if (boss.active) {
		sprite_draw(boss.sprite);
	}
}

// returns true if (x, y) is on the crcumference of a circle with 
// the given radius in the center of a box given by width x height 
bool onCircumference(int radius, int x, int y, int width, int height) {

	// get vector from the center to (x, y)
	int radiusX = round((width - 1) / 2) - x;
	int radiusY = round((height - 1) / 2) - y;

	// if on the circumference
	return (round(sqrt(pow(radiusX, 2) + pow(radiusY, 2))) == radius);
}

// returns true if (x, y) is in a circle with the given radius
// in the center of a box given by width x height 
bool inCircle(int radius, int x, int y, int width, int height) {

	// get vector from the center to (x, y)
	int radiusX = round((width - 1) / 2) - x;
	int radiusY = round((height - 1) / 2) - y;

	// if on the circumference
	return (round(sqrt(pow(radiusX, 2) + pow(radiusY, 2))) <= radius);
}