#ifndef BOSS_H
#define BOSS_H

typedef struct Boss Boss;

#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

// zdk
#include "cab202_sprites.h"
#include "cab202_graphics.h"
#include "cab202_timers.h"

#include "main.h"

#define BOSS_LOOP_RADIUS 10
#define BOSS_TRAVEL_SPEED (9 * 0.66666) // char/s^2
#define BOSS_SIZE_MIN 3
#define BOSS_SIZE_MAX 15
#define BOSS_SPAWN_TIME_RANGE ((1 * 60 * 1000) - (45 * 1000)) + (45 * 1000) // 45s to 2m

struct Boss {
	sprite_id sprite;
	int spriteRadius;
	int loopRadius;
	int loopStartY;
	bool apexReached;
	bool Looping;
	double destinationDy, destinationDx;
	bool active;
};

extern Boss boss;

// sets up the a boss with random attributes readdy for behavour updating
void setupBoss();

// frees memory allocations for the boss
void deleteBoss();

// updates the bosses behavour
void updateBoss();

// moves the boss in a curly wirly mannor
void moveBoss();

// Draws the a circular sprite to the boss bitmap with an arrow specifying the direction of travel
void drawBitmap();

// draws the boss if it is active
void drawBoss();

// returns true if (x, y) is on the crcumference of a circle with 
// the given radius in the center of a box given by width x height 
bool onCircumference(int radius, int x, int y, int width, int height);

// returns true if (x, y) is in a circle with the given radius
// in the center of a box given by width x height 
bool inCircle(int radius, int x, int y, int width, int height);

#endif // BOSS_H