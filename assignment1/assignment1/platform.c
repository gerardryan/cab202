#include "platform.h"

// removes all platforms and sets up the inital platform with default values
void setupInitalPlatform() {

	// clear platforms off screen
	while (platforms.head != NULL) {
		deletePlatform(removePlatform(&platforms, platforms.head, NULL));
	}

	// Setup
	initalPlatform = malloc(sizeof(Platform));
	char* bitmap = malloc(sizeof(char) * (PLATFORM_HEIGHT * PLATFORM_WIDTH));
	memset(bitmap, PLATFORM_CHAR, sizeof(char) * (PLATFORM_HEIGHT * PLATFORM_WIDTH));

	initalPlatform->deadly = false;
	initalPlatform->next = NULL;
	initalPlatform->sprite = sprite_create((screen_width() - PLATFORM_WIDTH) / 2,
										   screen_height() - (PLATFORM_HEIGHT + BOUNDARY_HEIGHT),
										   PLATFORM_WIDTH, PLATFORM_HEIGHT,
										   bitmap);
	initalPlatform->sprite->dy = PLATFORM_SPEED;

	addPlatformToTail(&platforms, initalPlatform);
}

// Update platform behavour such as spawning and moving
void updatePlatforms() {
	if (!gameRestarting) {
		movePlatforms();
		checkSpawnPlatform();
	}
}

// moves all the platforms up the screen
void movePlatforms() {
	Platform* precedingPlatform = NULL;
	Platform* platform = platforms.head;

	while (platform != NULL) {
		sprite_move(platform->sprite, (platform->sprite->dx * deltaTime) * currGameSpeed, (platform->sprite->dy * deltaTime) * currGameSpeed);

		// if going to be rendered off screen remove
		if (round(platform->sprite->y) <= (BOUNDARY_HEIGHT - 1)) {

			// if the inital platform is being removed don't leave it hanging
			if (platform == initalPlatform) {
				initalPlatform = NULL;
			}

			// remove and delete
			deletePlatform(removePlatform(&platforms, platform, precedingPlatform));

			// setup for next iteration then don't skip another
			if (precedingPlatform == NULL) { // if removed head
				platform = platforms.head;
			} else {
				platform = precedingPlatform->next;
			}
			continue;
		}

		platform = platform->next;
		if (precedingPlatform == NULL) { // if was on head
			precedingPlatform = platforms.head;
		} else {
			precedingPlatform = precedingPlatform->next;
		}
	}
}

// checks if a platform should be spawned and trys to spawn one if true
void checkSpawnPlatform() {
	bool isDeadly = rand() % 2; // 1 or 0, true or false
	bool trySucceed = false;

	// screen is full as first platform is now gone or we are nearing the need for required platforms
	if (initalPlatform == NULL || platforms.totalPlatforms - (rand() % NEED_VS_RANDOM_PLATFORM_BUFFER) >= MIN_SAFE_PLATFORMS + MIN_DEADLY_PLATFORMS) {
		if ((platforms.totalPlatforms - platforms.deadlyPlatforms) <= MIN_SAFE_PLATFORMS) {
			isDeadly = false;
			trySucceed = true;
		} else if (platforms.deadlyPlatforms <= MIN_DEADLY_PLATFORMS) {
			isDeadly = true;
			trySucceed = true;
		}
	}

	if (timer_expired(platformSpawn)) {
		Platform* temp = trySpawnPlatform(isDeadly, trySucceed, PLATFORM_MAX_SPAWNING_TIME);
		if (temp != NULL) {
			addPlatformToTail(&platforms, temp);
		}
	}
}

// Trys to spawm a platform of of type specified by isDeadly. if succeed is true will try and spawn until timeout
Platform* trySpawnPlatform(bool isDeadly, bool trySucceed, double timeout) {

	// chance to try and spawn if not trying to succeed
	if (trySucceed || (rand() % 2)) { // 1 or 0, true or false
		double x, y;
		int width, height;
		double startTime = get_current_time();

		do {
			// randomize try to spawn
			height = PLATFORM_HEIGHT;

			if (level < 2) {
				width = 7;
			} else {
				width = rand() % ((MAX_PLATFORM_WIDTH - MIN_PLATFORM_WIDTH) + 1) + MIN_PLATFORM_WIDTH;
			}

			x = rand() % (screen_width() - width);
			y = ((screen_height() - BOUNDARY_HEIGHT) - 1) - (1 - (platforms.head->sprite->y - (int)platforms.head->sprite->y)); // take decimal to normalize rendering

			// test collision 
			bool collision = false;
			Box targetBox;
			Box extendedBoundBox = {x - PLATFORM_H_SPAWN_DEADZONE,
				y - PLATFORM_V_SPAWN_DEADZONE,
				width + (PLATFORM_H_SPAWN_DEADZONE * 2),
				height + (PLATFORM_V_SPAWN_DEADZONE * 2)};

			for (Platform* i = platforms.head; i != NULL; i = i->next) {
				targetBox = (Box) { i->sprite->x, i->sprite->y, i->sprite->width, i->sprite->height };

				if (boxCollided(extendedBoundBox, targetBox)) {
					collision = true;
					break; // no point testing others
				}
			}

			if (!collision) {
				Platform* temp = malloc(sizeof(Platform));
				char* bitmap = malloc(sizeof(char) * (height * width));
				memset(bitmap, (isDeadly) ? DEADLY_PLATFORM_CHAR : PLATFORM_CHAR, sizeof(char) * (height * width));

				temp->deadly = isDeadly;
				temp->sprite = sprite_create(x, y, width, height, bitmap);
				sprite_turn_to(temp->sprite, 0, PLATFORM_SPEED);
				return temp;
			}
		} while (trySucceed && (get_current_time() - startTime) < timeout);
	}

	return NULL;
}

// Iterates the global platforms list and draws them to the screen
void drawPlatforms() {
	for (Platform* platform = platforms.head; platform != NULL; platform = platform->next) {
		sprite_draw(platform->sprite);
	}
}

// Adds the given platform to the tail of the given platform list and increments count vars
void addPlatformToTail(PlatformList* platforms, Platform* platform) {

	// set given platform's next to null in case i try and screw me over
	platform->next = NULL;

	// add to given lists head if empty
	if (platforms->head == NULL) {
		platforms->head = platform;
		platforms->tail = platform;

	} else { // add to given lists tail
		Platform* temp = platforms->tail;
		platforms->tail = platform;
		temp->next = platform;
	}

	platforms->totalPlatforms++;
	if (platform->deadly) {
		platforms->deadlyPlatforms++;
	}
}

// Removes the given platform from the given platform list, Given a non null precedingPlatform platform hastens removal
Platform* removePlatform(PlatformList* platforms, Platform* platform, Platform* precedingPlatform) {
	if (platforms == NULL || platform == NULL) {
		return NULL;
	} else if (precedingPlatform != NULL) {
		precedingPlatform->next = platform->next;
		if (platforms->tail == platform) {
			platforms->tail = precedingPlatform;
		}
	} else {
		if (platforms->head == platform) {
			platforms->head = platform->next;
		} else {

			// search for preceeding platform
			precedingPlatform = platforms->head;
			while (precedingPlatform->next != platform) {
				precedingPlatform = precedingPlatform->next;
			}

			precedingPlatform->next = platform->next;
			if (platforms->tail == platform) {
				platforms->tail = precedingPlatform;
			}
		}
	}

	// update count vars and return removed platform
	platforms->totalPlatforms--;
	if (platform->deadly) {
		platforms->deadlyPlatforms--;
	}
	return platform;
}

// Given a platform frees all associated memory
void deletePlatform(Platform* platform) {
	free(platform->sprite->bitmap);
	sprite_destroy(platform->sprite);
	free(platform);
}