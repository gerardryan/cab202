#ifndef PLATFORM_H
#define PLATFORM_H

typedef struct Platform Platform;
typedef struct PlatformList PlatformList;

#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>

// zdk
#include "cab202_sprites.h"
#include "cab202_timers.h"
#include "cab202_graphics.h"

#include "main.h"
#include "box.h"

#define PLATFORM_SPAWN_TIME 500 // ms
#define PLATFORM_MAX_SPAWNING_TIME 0.01 // s
#define PLATFORM_SPEED -2 // chars/second
#define NEED_VS_RANDOM_PLATFORM_BUFFER 7
#define MIN_SAFE_PLATFORMS 5
#define MIN_DEADLY_PLATFORMS 2
#define PLATFORM_HEIGHT 2
#define PLATFORM_WIDTH 7
#define PLATFORM_CHAR '='
#define DEADLY_PLATFORM_CHAR 'x'
#define PLATFORM_H_SPAWN_DEADZONE 4
#define PLATFORM_V_SPAWN_DEADZONE 3
#define MIN_PLATFORM_WIDTH 3
#define MAX_PLATFORM_WIDTH 10

struct Platform {
	sprite_id sprite;
	bool deadly;
	Platform* next;
};

struct PlatformList {
	Platform* head;
	Platform* tail;
	int deadlyPlatforms;
	int totalPlatforms;
};

// removes all platforms and sets up the inital platform with default values
void setupInitalPlatform();

// Update platform behavour such as spawning and moving
void updatePlatforms();

// moves all the platforms up the screen
void movePlatforms();

// checks if a platform should be spawned and trys to spawn one if true
void checkSpawnPlatform();

// Trys to spawm a platform of of type specified by isDeadly. if succeed is true will try and spawn until timeout
Platform* trySpawnPlatform(bool isDeadly, bool trySucceed, double timeout);

// Iterates the global platforms list and draws them to the screen
void drawPlatforms();

// Adds the given platform to the tail of the given platform list and increments count vars
void addPlatformToTail(PlatformList* platforms, Platform* platform);

// Removes the given platform from the given platform list, Given a non null precedingPlatform platform hastens removal
Platform* removePlatform(PlatformList* platforms, Platform* platform, Platform* precedingPlatform);

// Given a platform frees all associated memory
void deletePlatform(Platform* platform);

#endif // !PLATFORM_H