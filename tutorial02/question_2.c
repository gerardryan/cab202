/*	CAB202: Tutorial 2
*	Question 2 - Template
*
*	B.Talbot, March 2016
*	Queensland University of Technology
*/
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#include "cab202_graphics.h"
#include "cab202_timers.h"

/*
* Function declarations:
*	- 'has_bomb_hit()' must be completed to return true if the bomb coordinates will hit the base, false if not
*	- 'draw_base()' simply draws the base with edges at 25% and 75% of both screen width and screen height
*/
bool has_bomb_hit(int bomb_x, int bomb_y);
void draw_base();

const float base_min = 0.25f, base_max = 0.75f;

/*
* Main - nothing needs to be changed here
*/
int main() {
	// Seed the random number generator - based off the system clock so the seed is different every time
	time_t t;
	srand((unsigned) time(&t));

	// Initialise the graphics environment
	setup_screen();

	// Perform the main loop (draw base, then 'bomb' until the base has been hit)
	int bomb_x = 0, bomb_y = 0;
	draw_base();
	show_screen();
	int bombCount = 5;
	bool hasWon = true;
	while (!has_bomb_hit(bomb_x, bomb_y)) {
		char key = wait_char();
		if (bombCount > 0 && key == ' ') {
			bombCount--;
			draw_char(bomb_x, bomb_y, ' ');
			bomb_x = rand() % screen_width();
			bomb_y = rand() % screen_height();
			draw_char(bomb_x, bomb_y, 'x');
			show_screen();
			timer_pause(750);
		} else if (bombCount < 1) {
			hasWon = false;
			break;
		}
	}

	// Finish message
	if (hasWon) {
		draw_string(0, screen_height() - 1, "The base has been hit!");
	} else {
		draw_string(0, screen_height() - 1, "You have Lost!");
	}
	show_screen();
	timer_pause(5000);

	// Tidy up
	cleanup_screen();
}

/*
* Complete this function
*/
bool has_bomb_hit(int bomb_x, int bomb_y) {
	int scrH = screen_height();
	int scrW = screen_width();

	bool top = bomb_y > scrH * base_min;
	bool bottom = bomb_y < scrH * base_max;
	bool left = bomb_x > scrW * base_min;
	bool right = bomb_x < scrW * base_max;

	return (top && bottom && left && right);
}

/*
* Already complete - no changes needed
*/
void draw_base(int size) {
	for (int i = screen_width()*base_min; i< screen_width()*base_max; i++) {
		for (int ii = screen_height()*base_min; ii< screen_height()*base_max; ii++) {
			draw_char(i, ii, 'H');
		}
	}
}
