/*	CAB202: Tutorial 1
*	Question 3 - Template
*
*	B.Talbot, February 2016
*	Queensland University of Technology
*/
#include "cab202_graphics.h"
#include <ctype.h>

int vertLinePos = 0;
int horiLinePos = 0;
#define LINE_INCREMENT 1

int changeVertLinePos(int increment) {
	vertLinePos += increment;
	if (vertLinePos > screen_width() - 1) {
		vertLinePos = screen_width() - 1;
	} else if (vertLinePos < 0) {
		vertLinePos = 0;
	}
	return vertLinePos;
}

int changeHoriLinePos(int increment) {
	horiLinePos += increment;
	if (horiLinePos > screen_height() - 1) {
		horiLinePos = screen_height() - 1;
	} else if (horiLinePos < 0) {
		horiLinePos = 0;
	}
	return horiLinePos;
}

int main() {

	// Setup the screen and display a waiting message
	setup_screen();
	show_screen();
	
	horiLinePos = screen_height() / 2;
	vertLinePos = screen_width() / 2;

    while (1<2) {
		// Wait for the key press
		int key_in = get_char();

		// Clear the screen
		clear_screen();

		// Draw a word with the character if supported, otherwise print a
	    // string saying that the character is not supported.
		if (isdigit(key_in)) {
			draw_string(27, 11, "Numbers are unsupported");
		} else if (isalpha(key_in)) {
			switch(tolower(key_in)) {
				case 'w':
					changeHoriLinePos(-LINE_INCREMENT);
					break;
				
				case 's':
					changeHoriLinePos(LINE_INCREMENT);
					break;
				
				case 'a':
					changeVertLinePos(-LINE_INCREMENT);
					break;
				
				case 'd':
					changeVertLinePos(LINE_INCREMENT);
					break;
					
				default:
					draw_string(27, 11, "Unsupported letter");
			}
		}/* else {
			get_char(); // ignore second char
			key_in = get_char();
			switch(key_in) {
				case 'A':
					changeHoriLinePos(-LINE_INCREMENT);
					break;
				
				case 'B':
					changeHoriLinePos(LINE_INCREMENT);
					break;
				
				case 'D':
					changeVertLinePos(-LINE_INCREMENT);
					break;
				
				case 'C':
					changeVertLinePos(LINE_INCREMENT);
					break;
					
				default:
					draw_string(27, 11, "Unsupported Character");
			}
		}*/
		
    	// Draw from left\top to right\bottom
		for (int x = 0; x < vertLinePos; x++) {
			char px = '-';
			if (x % 2 == 0) {
				px = ' ';
			}
			draw_char(x, horiLinePos, px);
		}
		for (int y = 0; y <= screen_height(); y++) {
			char px = '|';
			if (y == horiLinePos) {
				px = '+';
			} else if ( y % 2 == 0) {
				px = ' ';
			}
			draw_char(vertLinePos, y, px);
		}
		for (int x = vertLinePos + 1; x <= screen_width(); x++) {
			char px = '-';
			if (x % 2 == 0) {
				px = ' ';
			}
			draw_char(x, horiLinePos, px);
		}
    }

    // Clean up the screen and return (we will NEVER make it here!)
    cleanup_screen();
    return 0;
}
