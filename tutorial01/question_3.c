/*	CAB202: Tutorial 1
*	Question 3 - Template
*
*	B.Talbot, February 2016
*	Queensland University of Technology
*/
#include "cab202_graphics.h"
#include <ctype.h>

int main() {

	// Setup the screen and display a waiting message
	setup_screen();
	draw_string(27, 11, "Waiting for key press...");
	show_screen();

    while (1<2) {
		// Wait for the key press
		int key_in = wait_char();

		// Clear the screen
		clear_screen();

		// Draw a word with the character if supported, otherwise print a
	    // string saying that the character is not supported.
		if (isdigit(key_in)) {
			draw_string(27, 11, "Numbers are unsupported");
		} else if (isalpha(key_in)) {
			switch(tolower(key_in)) {
				case 'a':
					draw_string(27, 11, "Apple");
					break;
				
				case 'b':
					draw_string(27, 11, "Banana");
					break;
				
				case 'c':
					draw_string(27, 11, "Cantaloupe");
					break;
				
				case 'd':
					draw_string(27, 11, "Date");
					break;
				
				case 'e':
					draw_string(27, 11, "Elderberry");
					break;
					
				default:
					draw_string(27, 11, "Unsupported letter");
			}
		} else {
			draw_string(27, 11, "Unsupported Character");
		}
    }

    // Clean up the screen and return (we will NEVER make it here!)
    cleanup_screen();
    return 0;
}
