/*	CAB202: Tutorial 1
*	Question 2 - Template
*
*	B.Talbot, February 2016
*	Queensland University of Technology
*/
#include <stdio.h>

/*
 * brief - counts from start to end inclusive by increment.
 * param - float start - the number inclusive to add and start from.
 * param - float end - the number inclusive to add and finish at.
 * param - float increment - the value to add to get the next number in the list.
 */
void count(float start, float end, float increment);

int main() {
    // Count from 0 to 12
    printf("\nFROM 0 TO 12:\n");
    count(0, 12, 1);

    // Count from 5 to 10
    printf("\nFROM 5 TO 10:\n");
    count(5, 10, 1);

    // Count from 5 to 0
    printf("\nFROM 5 to 0:\n");
    count(5, 0, -1);

    // Count in 2's, from 10 to 30
    printf("\nIN 2's, FROM 10 to 30:\n");
    count(10, 30, 2);

    // Count in 0.25's, from 0 to 2
    printf("\nIN 0.25's, FROM 0 to 2:\n");
    count(0, 2, 0.25);

    // Return from main
    return 0;
}

/*
 * brief - counts from start to end inclusive by increment.
 * param - float start - the number inclusive to add and start from.
 * param - float end - the number inclusive to add and finish at.
 * param - float increment - the value to add to get the next number in the list.
 */
void count(float start, float end, float increment) {
	if (end < start) {
		if (increment > 0) {
			printf("\nERROR: increment needs to be negative to reach end\n");
			return;
		}
		for (float i = start; i >= end; i += increment) {
			printf("%g, ", i);
		}
		printf("\n");
	} else if (end > start) {
		if (increment < 0) {
			printf("\nERROR: increment needs to be positive to reach end\n");
			return;
		}
		for (float i = start; i <= end; i += increment) {
			printf("%g, ", i);
		}
		printf("\n");
	} else {
		// no need to count start/end is the only number
		printf("%g", start);
	}
}



