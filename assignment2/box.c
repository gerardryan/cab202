#include "box.h"

// Returns weather box 1 and box 2 would/have collided
unsigned char boxCollided(Box b1, Box b2) {
	unsigned char b1Left = round(b1.x);
	unsigned char b1Right = b1Left + round(b1.width - 1);
	unsigned char b1Top = round(b1.y);
	unsigned char b1Bottom = b1Top + round(b1.height - 1);

	unsigned char b2Left = round(b2.x);
	unsigned char b2Right = b2Left + round(b2.width - 1);
	unsigned char b2Top = round(b2.y);
	unsigned char b2Bottom = b2Top + round(b2.height - 1);

	if (b1Left <= b2Right && b1Right >= b2Left && b1Top <= b2Bottom && b1Bottom >= b2Top) { // if collision
		unsigned char collisions = 0;
		collisions ^= (-(b1Bottom >= b2Top && b1Bottom <= b2Bottom) ^ collisions) & TOP_COLLISION_BIT_MASK; // b1 collided with b2's top
		collisions ^= (-(b1Top <= b2Bottom && b1Top >= b2Top) ^ collisions) & BOTTOM_COLLISION_BIT_MASK; // b1 collided with b2's bottom
		collisions ^= (-(!(collisions & (TOP_COLLISION_BIT_MASK | BOTTOM_COLLISION_BIT_MASK))) ^ collisions) & HEIGHT_ENCLOSING_COLLISION_BIT_MASK; // b1 is enclosing the height of b2

		collisions ^= (-(b1Right >= b2Left && b1Right <= b2Right) ^ collisions) & LEFT_COLLISION_BIT_MASK; // b1 collided with b2's left
		collisions ^= (-(b1Left <= b2Right && b1Left >= b2Left) ^ collisions) & RIGHT_COLLISION_BIT_MASK; // b1 collided with b2's right
		collisions ^= (-(!(collisions & (LEFT_COLLISION_BIT_MASK | RIGHT_COLLISION_BIT_MASK))) ^ collisions) & WIDTH_ECCLOSING_COLLISION_BIT_MASK; // b1 is enclosing the width of b2
		return collisions;
	}
	return 0;
}