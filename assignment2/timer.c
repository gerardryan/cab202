#include "main.h"
#include "timer.h"

// initalizes and starts a timer
void initTimer(Timer* timer, double seconds) {
	timer->timerStart = get_system_time();
	timer->seconds = seconds;
}

// returns if the given timer is expired, if true resets the timer
unsigned char timerExpired(Timer* timer) {
	double now = get_system_time();
	if (now >= timer->timerStart + timer->seconds) {
		timer->timerStart = now;
		return 1;
	}
	return 0;
}

// resets the given timer
void resetTimer(Timer* timer) {
	timer->timerStart = get_system_time();
}