#include <stdlib.h>
#include <math.h>

#include "graphics.h"
#include "sprite.h"

#include "my_macros.h"
#include "wall.h"
#include "aliens.h"
#include "player.h"
#include "box.h"
#include "main.h"
#include "lcd.h"
#include "bullet.h"

BulletList bullets;

// updates the bullets
void updateBullets() {
	moveBullets();
	bulletsCollision();
}

// iterates through all the bullets and checks collision
void bulletsCollision() {
	for (Bullet* bullet = bullets.head; bullet != NULL; bullet = bullet->next) {
		Box bulletBox = {bullet->x, bullet->y, 1.0f, 1.0f};
		if (bullet->killFlags & (1 << KILL_ALIENS)) {
			for (unsigned char i = 0; i < NUM_ALIENS; i++) {
				if (aliens[i].alive) {
					if (boxCollided(bulletBox, (Box) { aliens[i].sprite.x, aliens[i].sprite.y, aliens[i].sprite.width, aliens[i].sprite.height })) {
						unsigned char byte = round(bullet->y) - aliens[i].sprite.y;
						unsigned char bit = 7 - (round(bullet->x) - aliens[i].sprite.x);
						if ((aliens[i].sprite.bitmap[byte] >> bit) & 1) {
							aliens[i].alive = 0;
							player.score++;
							free(removeBullet(bullet, NULL));
							if (player.score >= NUM_ALIENS) {
								quit = 1;
							}
						}
					}
				}
			}
		}
		if (bullet->killFlags & (1 << KILL_PLAYER)) {
			if (boxCollided(bulletBox, (Box) { player.sprite.x, player.sprite.y, player.sprite.width, player.sprite.height })) {
				unsigned char byte = round(bullet->y) - player.sprite.y;
				unsigned char bit = 7 - (round(bullet->x) - player.sprite.x);
				if ((player.sprite.bitmap[byte] >> bit) & 1) {
					player.lives--;
					free(removeBullet(bullet, NULL));
					if (player.lives <= 0) {
						quit = 1;
					}
				}
			}
		}
		if (bullet->killFlags & (1 << KILL_WALL)) {
			if (level > 1) {
				for (unsigned char i = 0; i < NUM_WALLS; i++) {
					if (boxCollided(bulletBox, (Box) { walls[i].sprite.x, walls[i].sprite.y, walls[i].sprite.width, walls[i].sprite.height })) {
						unsigned char byte = round(bullet->y) - walls[i].sprite.y;
						unsigned char bit = 7 - (round(bullet->x) - walls[i].sprite.x);
						if ((walls[i].bitmap[byte] >> bit) & 1) {
							CLEAR_BIT(walls[i].bitmap[byte], bit);
							free(removeBullet(bullet, NULL));
						}
					}
				}
			}
		}
	}
}

// iterates through all the bullets and updates them
void moveBullets() {
	Bullet* precedingBullet = NULL;
	Bullet* bullet = bullets.head;

	while (bullet != NULL) {
		bullet->dx += bullet->a;
		bullet->x += bullet->dx * deltaTime;
		bullet->y += bullet->dy * deltaTime;

		// if going to be rendered off screen remove
		if (round(bullet->y) <= SCREEN_MAX_H || round(bullet->y) >= LCD_Y
			|| round(bullet->x) < 0 || round(bullet->x) >= LCD_X) {

			// remove and delete
			free(removeBullet(bullet, precedingBullet));

			// setup for next iteration then don't skip another
			if (precedingBullet == NULL) { // if removed head
				bullet = bullets.head;
			} else {
				bullet = precedingBullet->next;
			}
			continue;
		}

		bullet = bullet->next;
		if (precedingBullet == NULL) { // if was on head
			precedingBullet = bullets.head;
		} else {
			precedingBullet = precedingBullet->next;
		}
	}
}

// iterates through all the bullets and draws them
void drawBullets() {
	for (Bullet* bullet = bullets.head; bullet != NULL; bullet = bullet->next) {
		set_pixel(round(bullet->x), round(bullet->y), 1);
	}
}

// Adds the given bullet to the tail of bullets
void addBulletToTail(Bullet* bullet) {

	// set given bullet's next to null in case i try and screw me over
	bullet->next = NULL;

	// add to given lists head if empty
	if (bullets.head == NULL) {
		bullets.head = bullet;
		bullets.tail = bullet;

	} else { // add to given lists tail
		Bullet* temp = bullets.tail;
		bullets.tail = bullet;
		temp->next = bullet;
	}
}

// Removes the given bullet from bullets, Given a non null precedingBullet bullet hastens removal
Bullet* removeBullet(Bullet* bullet, Bullet* precedingBullet) {
	if (bullet == NULL) {
		return NULL;
	} else if (precedingBullet != NULL) {
		precedingBullet->next = bullet->next;
		if (bullets.tail == bullet) {
			bullets.tail = precedingBullet;
		}
	} else {
		if (bullets.head == bullet) {
			bullets.head = bullet->next;
		} else {

			// search for preceeding bullet
			precedingBullet = bullets.head;
			while (precedingBullet->next != bullet) {
				precedingBullet = precedingBullet->next;
			}

			precedingBullet->next = bullet->next;
			if (bullets.tail == bullet) {
				bullets.tail = precedingBullet;
			}
		}
	}
	return bullet;
}