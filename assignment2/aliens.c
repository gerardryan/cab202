#include "stdlib.h"
#include "math.h"
#include "sprite.h"
#include "my_sprites.h"
#include "lcd.h"
#include "avr/io.h"

#include "player.h"
#include "box.h"
#include "my_macros.h"
#include "timer.h"
#include "bullet.h"
#include "main.h"
#include "aliens.h"

Timer alienShootTime;

// sets up the aliens
void setupAliens() {
	for (unsigned char i = 0; i < NUM_ALIENS; i++) {
		init_sprite(&aliens[i].sprite, (LCD_X - ((ALIEN_W + 1) * ALIENS_A_ROW)) / 2 + (i % ALIENS_A_ROW) * (ALIEN_W + 1), SCREEN_MAX_H + 1 + (i % 3) * (ALIEN_H + 1), ALIEN_W, ALIEN_H, alienSprite);
		aliens[i].alive = 1;
		if (level == 1) {
			aliens[i].sprite.dx = ALIEN_SPEED_X;
			aliens[i].sprite.dy = 0;
		} else if (level == 2) {
			aliens[i].sprite.dx = ALIEN_SPEED_X;
			aliens[i].sprite.dy = ALIEN_SPEED_Y;
		} else if (level == 3) {
			aliens[i].sprite.dx = (ALIEN_SPEED_X + ((i % NUM_ROWS) + 1));
			aliens[i].sprite.dy = ((ALIEN_SPEED_Y + ((i % NUM_ROWS) + 1)) * -1);
		}
	}
	initTimer(&alienShootTime, SHOOT_TIME);
}

// Updates the aliens
void updateAliens() {
	moveAliens();
	dropBullet();
}

// iterates through all the aliens and moves them
void moveAliens() {
	unsigned char changeDirFlags = 0;
	for (unsigned char i = 0; i < NUM_ALIENS; i++) {
		if (aliens[i].alive) {
			if ((round(aliens[i].sprite.x) <= 2 && aliens[i].sprite.dx < 0) || (round(aliens[i].sprite.x) + aliens[i].sprite.width >= LCD_X && aliens[i].sprite.dx > 0)) {
				if (level == 3) {
					SET_BIT(changeDirFlags, (i % NUM_ROWS));
				} else {
					SET_BIT(changeDirFlags, 0);
				}
			}
			if ((round(aliens[i].sprite.y) <= SCREEN_MAX_H && aliens[i].sprite.dy < 0) || (round(aliens[i].sprite.y) + aliens[i].sprite.height >= LCD_Y - 16 && aliens[i].sprite.dy > 0)) {
				if (level == 3) {
					SET_BIT(changeDirFlags, ((i % NUM_ROWS) + NUM_ROWS));
				} else {
					SET_BIT(changeDirFlags, CHANGE_DIR_Y);
				}
			}
			if (level == 3) { // other rows
				Box alienI = {0, aliens[i].sprite.y, LCD_X, aliens[i].sprite.height};
				for (unsigned char j = 0; j < NUM_ALIENS; j++) {
					if (aliens[j].alive) {
						if ((i % NUM_ROWS) != (j % NUM_ROWS)) { // not the same row
							Box alienJ = {0, aliens[j].sprite.y, LCD_X, aliens[j].sprite.height};
							if (boxCollided(alienI, alienJ)) {
								SET_BIT(changeDirFlags, ((i % NUM_ROWS) + NUM_ROWS));
							}
						}
					}
				}
			}
		}
	}

	for (unsigned char i = 0; i < NUM_ALIENS; i++) {

		// up or down should toggle
		if (level == 3 && CHECK_BIT(changeDirFlags, ((i % NUM_ROWS) + NUM_ROWS))) {
			aliens[i].sprite.dy *= -1;
		} else if (level < 3 && CHECK_BIT(changeDirFlags, CHANGE_DIR_Y)) {
			aliens[i].sprite.dy *= -1;
		}

		// left or right should toggle
		if (level == 3 && CHECK_BIT(changeDirFlags, (i % NUM_ROWS))) {
			aliens[i].sprite.dx *= -1;
		} else if (level < 3 && CHECK_BIT(changeDirFlags, 0)) {
			aliens[i].sprite.dx *= -1;
		}

		aliens[i].sprite.x += aliens[i].sprite.dx * deltaTime;
		aliens[i].sprite.y += aliens[i].sprite.dy * deltaTime;
	}
}

// randomally drops a bullet
void dropBullet() {
	if (timerExpired(&alienShootTime)) {
		for (unsigned char i = 0; i < NUM_ALIENS; i++) {
			if (aliens[i].alive) {
				if ((rand() % 99 + 1) < (ALIEN_SHOOT_CHANCE + (15 * (player.score / NUM_ALIENS)))) {
					Bullet* temp = malloc(sizeof(Bullet));
					temp->killFlags = (1 << KILL_PLAYER) | (1 << KILL_WALL);
					temp->x = aliens[i].sprite.x + (aliens[i].sprite.width / 2);
					temp->y = aliens[i].sprite.y + aliens[i].sprite.height;
					temp->dy = BULLET_SPEED;
					temp->dx = 0;
					temp->a = 0;
					addBulletToTail(temp);
				}
			}
		}
	}
}

// Draws the aliens
void drawAliens() {
	for (unsigned char i = 0; i < NUM_ALIENS; i++) {
		if (aliens[i].alive) {
			draw_sprite(&aliens[i].sprite);
		}
	}
}