#ifndef _ALIENS_H_
#define _ALIENS_H_

#define NUM_ALIENS 15
#define ALIENS_A_ROW 5
#define ALIEN_W 8
#define ALIEN_H 6
#define ALIEN_SHOOT_CHANCE 25
#define ALIEN_SPEED_Y 1
#define ALIEN_SPEED_X 5

// swarm movement defines
#define CHANGE_DIR_Y 7
#define NUM_ROWS 3

typedef struct Aliens {
	Sprite sprite;
	unsigned char alive;
} Alien;

Alien aliens[NUM_ALIENS];

// sets up the aliens
void setupAliens();

// Updates the aliens
void updateAliens();

// iterates through all the aliens and moves them
void moveAliens();

// randomally drops a bullet
void dropBullet();

// Draws the aliens
void drawAliens();

#endif // !ALIENS_H