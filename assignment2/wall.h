#ifndef _WALL_H_
#define _WALL_H_

#define WALL_H 5
#define WALL_W 8

#define WALL_Y (LCD_Y / 2) + 11
#define WALL_OFFSET 12

#define NUM_WALLS 3

typedef struct Wall {
	Sprite sprite;
	unsigned char bitmap[WALL_H];
} Wall;

Wall walls[NUM_WALLS];

// sets up 3 destructable walls
void setupWalls();

// Draws the walls
void drawWalls();

#endif // !_WALL_H_