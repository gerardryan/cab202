#include "my_sprites.h"

unsigned char heartSprite[6] = {
	0b01101100,
	0b11111110,
	0b11111110,
	0b01111100,
	0b00111000,
	0b00010000
};

unsigned char alienSprite[6] = {
	0b10000010,
	0b11101110,
	0b10111010,
	0b11010110,
	0b01111100,
	0b01010100
};

unsigned char playerSprite[6] = {
	0b00001000,
	0b00001000,
	0b00011000,
	0b11111110,
	0b11111111,
	0b01111110
};

unsigned char wallSprite[5] = {
	0b01111110,
	0b11111111,
	0b11111111,
	0b11111111,
	0b11000011
};