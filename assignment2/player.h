#ifndef _PLAYER_H_
#define _PLAYER_H_

#define PLAYER_H 6
#define PLAYER_W 8

#define LEFT_DIR 1
#define RIGHT_DIR 2

#define PLAYER_SPEED 10

typedef struct Player {
	Sprite sprite;
	unsigned char lives;
	unsigned char score;
} Player;

Player player;

extern int playerTargetX;

// sets up the player
void setupPlayer();

// Updates the player
void updatePlayer();

// moves the player in the given direction
void movePlayer(unsigned char direction);

// causes the player to shoot
void playerShoot();

// Draws the player
void drawPlayer();

#endif // !PLAYER_H