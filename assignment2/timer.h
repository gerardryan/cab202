#ifndef _TIMER_H_
#define _TIMER_H_

typedef struct Timer {
	double timerStart;
	double seconds;
} Timer;

// initalizes and starts a timer
void initTimer(Timer* timer, double seconds);

// returns if the given timer is expired, if true resets the timer
unsigned char timerExpired(Timer* timer);

// resets the given timer
void resetTimer(Timer* timer);

#endif // !TIMER_H