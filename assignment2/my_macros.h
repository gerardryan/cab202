#ifndef MORE_MACROS_H

#define SET_BIT_TO(reg, pin, val) (reg ^= ((val) ? ~reg : reg) & 1 << pin)
#define SET_BIT(reg, pin) (reg |= 1 << pin)
#define CLEAR_BIT(reg, pin) (reg &= ~(1 << pin))
#define CHECK_BIT(reg, pin) (reg >> pin & 1)
#define TOGGLE_BIT(reg, pin) (reg ^= 1 << pin)

#define HIGH 1
#define LOW 0

// I/O
#define I 0
#define O 1

#define LSB 0
#define MSB 7

#endif // !MORE_MACROS_H