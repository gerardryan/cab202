#include <stdlib.h>
#include <avr\io.h>
#include <math.h>

#include "lcd.h"

#include "main.h"
#include "timer.h"
#include "my_macros.h"
#include "bullet.h"
#include "my_sprites.h"
#include "sprite.h"
#include "player.h"

Timer playerShootTime;
int playerTargetX = LCD_X / 2;

// sets up the player
void setupPlayer() {
	player.score = 0;
	player.lives = 3;
	init_sprite(&player.sprite, (LCD_X - PLAYER_W) / 2, LCD_Y - PLAYER_H - 1, PLAYER_W, PLAYER_H, playerSprite);
	initTimer(&playerShootTime, SHOOT_TIME);
}

// Updates the player
void updatePlayer() {
	if (level > 1 && player.sprite.x != playerTargetX) {
		if (round(player.sprite.x) < playerTargetX - 3) {
			player.sprite.dx = PLAYER_SPEED;
		} else if (round(player.sprite.x) > playerTargetX + 3) {
			player.sprite.dx = -PLAYER_SPEED;
		} else {
			player.sprite.dx = 0;
			player.sprite.x = playerTargetX;
		}
	}

	player.sprite.x += player.sprite.dx * deltaTime;
	if (player.sprite.x <= 1) {
		player.sprite.x = 1;
	} else if (player.sprite.x + player.sprite.width >= LCD_X) {
		player.sprite.x = LCD_X - player.sprite.width - 1;
	}
}

// moves the player in the given direction
void movePlayer(unsigned char direction) {
	if (direction == RIGHT_DIR) {
		player.sprite.x++; //dx = PLAYER_SPEED;
	} else if (direction == LEFT_DIR) {
		player.sprite.x--; //dx = -PLAYER_SPEED;
	}
}

// causes the player to shoot
void playerShoot() {
	if (timerExpired(&playerShootTime)) {
		Bullet* temp = malloc(sizeof(Bullet));
		temp->killFlags = (1 << KILL_ALIENS) | (1 << KILL_WALL);
		temp->x = player.sprite.x + (player.sprite.width / 2);
		temp->y = player.sprite.y;
		temp->dy = -BULLET_SPEED;
		temp->dx = 0;
		if (level == 3) {
			temp->a = (double)((adcRead(0) * (double)0.25 / (double)MAX_ADC) - (double)0.12);
		} else {
			temp->a = 0;
		}
		addBulletToTail(temp);
	}
}

// Draws the player
void drawPlayer() {
	draw_sprite(&player.sprite);
}