#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>

#include "lcd.h"
#include "graphics.h"
#include "cpu_speed.h"
#include "sprite.h"

#include "wall.h"
#include "timer.h"
#include "my_macros.h"
#include "player.h"
#include "aliens.h"
#include "bullet.h"
#include "my_sprites.h"
#include "main.h"

const double TIMER1_FRAC = 1.0 / FREQ;
const double TIMER1_OVER = 1.0 / FREQ * 0xffff;

volatile unsigned char buttonHistory[NUM_BUTTONS];
volatile unsigned char button;
volatile unsigned char firstButtonUp;
volatile unsigned char firstButtonDown;

volatile unsigned long systemTime = 0;

double deltaTime = 1.0 / FREQ;
unsigned char level = 1;
unsigned char quit = 0;

char stringBuf[80];
Sprite livesSymbol;
Timer refreshRate;

int main() {

	setup();

	double loopStartTime;
	while (!quit) {
		loopStartTime = get_system_time();

		checkInput();

		update();

		if (timerExpired(&refreshRate)) {
			clear_screen();
			draw();
			show_screen();
		}

		deltaTime = get_system_time() - loopStartTime;
	}

	draw_string((LCD_X - 45) / 2, (LCD_Y - 8) / 2, "GAME OVER");
	show_screen();
}

// Level select
void selectLevel() {
	unsigned char start = 0;
	while (!start) {

		clear_screen();
		drawUI();
		draw_string((LCD_X - 60) / 2, (LCD_Y - 8) / 2, "SELECT LEVEL");
		show_screen();

		if (checkButtonDown(BTN_RIGHT)) {
			level++;
			level = (level > MAX_LEVEL) ? 1 : level;
		}
		if (checkButtonDown(BTN_LEFT)) {
			start = 1;
		}
	}
}

// sets up the game and hardware
void setup() {

	// hardware & UI
	initializeHardware();
	initTimer(&refreshRate, REFRESH_RATE);
	init_sprite(&livesSymbol, 34, 0, 8, 6, heartSprite);

	selectLevel();

	// game state
	setupPlayer();
	setupAliens();
	setupWalls();
}

// initalizes Hardware
void initializeHardware() {
	set_clock_speed(CPU_8MHz);

	// setup the LCD screen
	lcd_init(LCD_DEFAULT_CONTRAST);
	SET_BIT_TO(DDRC, PC7, O);
	SET_BIT(PORTC, PC7); // LCD LED ON

	// setup Switch
	SET_BIT_TO(DDRD, PD1, I); // UP
	SET_BIT_TO(DDRB, PB1, I); // LEFT
	SET_BIT_TO(DDRB, PB7, I); // DOWN
	SET_BIT_TO(DDRD, PD0, I); // RIGHT

	// setup buttons
	SET_BIT_TO(DDRF, PF6, I); // LEFT 
	SET_BIT_TO(DDRF, PF5, I); // RIGHT

	// setup potentiometers / ADC
	ADMUX = (1 << REFS0);
	ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);	// 8000000/128 = 62500

	// setup timers
	CLEAR_BIT(TCCR0B, WGM02); // normal mode
	SET_BIT(TCCR0B, CS02); CLEAR_BIT(TCCR1B, CS01); CLEAR_BIT(TCCR1B, CS00); // overflows every ~2 miliseconds
	SET_BIT(TIMSK0, TOIE0); // Interrupt
	CLEAR_BIT(TCCR1B, WGM13); CLEAR_BIT(TCCR1B, WGM12); // normal mode
	CLEAR_BIT(TCCR1B, CS12); CLEAR_BIT(TCCR1B, CS11); SET_BIT(TCCR1B, CS10); // overflows every ~8.1 miliseconds
	SET_BIT(TIMSK1, TOIE1); // Interrupt

	// Globally enable interrupts
	sei();
}

// fires actions baised off of input
void checkInput() {
	if (checkButtonDown(BTN_DPAD_UP)) {
		playerShoot();
	}

	if (level == 1) { // L1 movement
		if (checkButtonUp(BTN_RIGHT)) {
			movePlayer(RIGHT_DIR);
		} else if (checkButtonUp(BTN_LEFT)) {
			movePlayer(LEFT_DIR);
		}
	} else if (level > 1) {
		playerTargetX = (adcRead(1) * (double)(LCD_X - PLAYER_W)) / MAX_ADC;
	}
}

// updates the game state
void update() {
	updateAliens();
	updatePlayer();
	updateBullets();
}

// draws the game state
void draw() {
	drawAliens();
	drawPlayer();
	drawWalls();
	drawBullets();
	drawUI();
}

// draws the user interface
void drawUI() {
	sprintf(stringBuf, "%u", player.score);
	draw_string(0, 0, stringBuf);

	draw_sprite(&livesSymbol);
	sprintf(stringBuf, "%u", player.lives);
	draw_string((LCD_X / 2) + 1, 0, stringBuf);

	sprintf(stringBuf, "L%u", level);
	draw_string(LCD_X - 10, 0, stringBuf);

	draw_line(0, 8, LCD_X - 1, 8);

	// border
	draw_line(0, LCD_Y - 1, LCD_X - 1, LCD_Y - 1);
	draw_line(0, 8, 0, LCD_Y - 1);
	draw_line(LCD_X - 1, 8, LCD_X - 1, LCD_Y - 1);
}

double get_system_time() {
	return systemTime * TIMER1_OVER + TIMER1_FRAC * (double)TCNT1;
}

// true when the given button is down
unsigned char checkButton(unsigned char btn) {
	return CHECK_BIT(button, btn);
}

// true for the first check after the button has changed to up
unsigned char checkButtonUp(unsigned char btn) {
	if (CHECK_BIT(firstButtonUp, btn)) {
		CLEAR_BIT(firstButtonUp, btn);
		return 1;
	}
	return 0;
}

// true for the first check after the button has changed to down
unsigned char checkButtonDown(unsigned char btn) {
	if (CHECK_BIT(firstButtonDown, btn)) {
		CLEAR_BIT(firstButtonDown, btn);
		return 1;
	}
	return 0;
}

// returns the analogue to digital conversion of the specified channel
unsigned short adcRead(unsigned char channel) {
	channel &= 0b00000111;
	ADMUX = (ADMUX & 0b11111000) | channel;

	// start and wait
	SET_BIT(ADCSRA, ADSC);
	while (CHECK_BIT(ADCSRA, ADSC));

	return (ADC);
}

// Interrupts
ISR(TIMER1_OVF_vect) {
	systemTime++;
}

ISR(TIMER0_OVF_vect) {
	for (unsigned char i = 0; i < NUM_BUTTONS; i++)
		buttonHistory[i] <<= 1;

	// store switch state
	SET_BIT_TO(buttonHistory[BTN_DPAD_UP], LSB, CHECK_BIT(PIND, PD1)); // UP
	SET_BIT_TO(buttonHistory[BTN_DPAD_LEFT], LSB, CHECK_BIT(PINB, PB1)); // LEFT
	SET_BIT_TO(buttonHistory[BTN_DPAD_DOWN], LSB, CHECK_BIT(PINB, PB7)); // DOWN
	SET_BIT_TO(buttonHistory[BTN_DPAD_RIGHT], LSB, CHECK_BIT(PIND, PD0)); // RIGHT

	// store buttons states
	SET_BIT_TO(buttonHistory[BTN_LEFT], LSB, CHECK_BIT(PINF, PF6)); // LEFT 
	SET_BIT_TO(buttonHistory[BTN_RIGHT], LSB, CHECK_BIT(PINF, PF5)); // RIGHT

	for (unsigned char i = 0; i < NUM_BUTTONS; i++) {
		if (CHECK_BIT(button, i)) { // button state down
			if (buttonHistory[i] == 0x00) { // button history is up
				SET_BIT_TO(button, i, BTN_STATE_UP);
				SET_BIT(firstButtonUp, i);
			}
		} else { // button state up
			if (buttonHistory[i] == 0xff) { // button history is down 
				SET_BIT_TO(button, i, BTN_STATE_DOWN);
				SET_BIT(firstButtonDown, i);
			}
		}
	}
}