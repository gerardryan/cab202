#ifndef _BULLET_H_
#define _BULLET_H_

typedef struct Bullet Bullet;
typedef struct BulletList BulletList;

#define KILL_ALIENS 0
#define KILL_PLAYER 1
#define KILL_WALL 2

#define BULLET_SPEED 2.0

struct Bullet {
	float x, y, dx, dy, a;
	unsigned char killFlags;
	Bullet* next;
};

struct BulletList {
	Bullet* head;
	Bullet* tail;
};

// updates the bullets
void updateBullets();

// iterates through all the bullets and checks collision
void bulletsCollision();

// iterates through all the bullets and moves them
void moveBullets();

// iterates through all the bullets and draws them
void drawBullets();

// Adds the given bullet to the tail of bullets
void addBulletToTail(Bullet* bullet);

// Removes the given bullet from bullets, Given a non null precedingBullet bullet hastens removal
Bullet* removeBullet(Bullet* bullet, Bullet* precedingBullet);

#endif // !BULLET_H