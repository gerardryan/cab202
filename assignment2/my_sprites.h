#ifndef _MY_SPRITES_H_
#define _MY_SPRITES_H_

extern unsigned char heartSprite[6];

extern unsigned char alienSprite[6];

extern unsigned char playerSprite[6];

extern unsigned char wallSprite[5];

#endif // !MY_SPRITES_H