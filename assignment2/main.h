#ifndef _MAIN_H_
#define _MAIN_H_

#define NUM_BUTTONS 6

#define BTN_DPAD_LEFT 0
#define BTN_DPAD_RIGHT 1
#define BTN_DPAD_UP 2
#define BTN_DPAD_DOWN 3
#define BTN_LEFT 4
#define BTN_RIGHT 5

#define BTN_STATE_UP 0
#define BTN_STATE_DOWN 1

#define FREQ 8000000.0

#define SCREEN_MAX_H 9
#define REFRESH_RATE (1.0 / 60.0)
#define MAX_LEVEL 3

#define SHOOT_TIME 6 // s

#define MAX_ADC 1023.0

extern volatile unsigned long systemTime;
extern double deltaTime;
extern unsigned char level;
extern unsigned char quit;

// Level select
void selectLevel();

// sets up the game and hardware
void setup();

// initalizes Hardware
void initializeHardware();

// fires actions baised off of input
void checkInput();

// updates the game state
void update();

// draws the game state
void draw();

// draws the user interface
void drawUI();

// gets the time since application start
double get_system_time();

// true when the given button is down
unsigned char checkButton(unsigned char button);

// true for the first check after the button has changed to up
unsigned char checkButtonUp(unsigned char button);

// true for the first check after the button has changed to down
unsigned char checkButtonDown(unsigned char button);

// returns the analogue to digital conversion of the specified channel
unsigned short adcRead(unsigned char channel);

#endif // !MAIN_H