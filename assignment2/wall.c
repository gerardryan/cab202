#include "main.h"
#include "string.h"
#include "lcd.h"
#include "my_sprites.h"
#include "sprite.h"
#include "wall.h"

// sets up 3 destructable walls
void setupWalls() {
	if (level > 1) {
		for (unsigned char i = 0; i < NUM_WALLS; i++) {
			memccpy(&walls[i].bitmap, &wallSprite, 1, sizeof(walls[i].bitmap));
			init_sprite(&walls[i].sprite, ((LCD_X - WALL_W) / 3 * i) + WALL_OFFSET, WALL_Y, WALL_W, WALL_H, walls[i].bitmap);
		}
	}
}

// Draws the walls
void drawWalls() {
	if (level > 1) {
		for (unsigned char i = 0; i < NUM_WALLS; i++) {
			draw_sprite(&walls[i].sprite);
		}

	}
}