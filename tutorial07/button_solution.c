/* File: button_solution.c
 * Description: C program for the ATMEL AVR microcontroller (ATmega32 chip)
 * Turn on Teensy LED with button

 * Teensy PewPew

 * Includes (pretty much compulsory for using the Teensy this semester)
 * 	- avr/io.h: port and pin definitions (i.e. DDRB, PORTB, PB1, etc)
 *
 *	- cpu_speed.h: macros for forcing the CPU speed to 8MHz (nothing else should ever be used!)
 */



// AVR header file for all registers/pins
#include "avr\io.h"

#include "cpu_speed.h"

#define SET_BIT_TO(reg, pin, val) (reg ^= ((val) ? ~reg : reg) & 1 << pin)
//#define SET_BIT_TO(reg, pin, val) (reg ^= (-val ^ reg) & 1 << pin)
#define TOGGLE_BIT(reg, pin) (reg ^= 1 << pin)
#define CHECK_BIT(reg, pin) (reg >> pin & 1)

int main(void){
    
    // Set the CPU speed to 8MHz (you must also be compiling at 8MHz)
    set_clock_speed(CPU_8MHz);
    
     // SW0 and SW1 are connected to pins F6 and F5. Configure all pins as Input(0)
	 DDRF = 0;
    
    // Teensy LED is connected to B2, configure pin 2 as an output(1)
     SET_BIT_TO(DDRB, PORTB2, 1);
    
     // turn OFF LED initially
	 SET_BIT_TO(PORTB, PORTB2, 0);
	
    while(1){
         if(CHECK_BIT(PINF, PINF5)){
			SET_BIT_TO(PORTB, PORTB2, 1);
		 } else {
			SET_BIT_TO(PORTB, PORTB2, 0);
		 }
     }
     return 0;
}
