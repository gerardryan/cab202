#include "avr\io.h"
#include "cpu_speed.h"
#include "led_pwm.h"

#define SET_BIT_TO(reg, pin, val) (reg ^= ((val) ? ~reg : reg) & 1 << pin)
#define CHECK_BIT(reg, pin) (reg >> pin & 1)

int main() {
	set_clock_speed(CPU_8MHz);
	SET_BIT_TO(DDRB, PB2, 1); // SET LED0 Out
	SET_BIT_TO(DDRF, PF5, 0); // SET SW3 IN
	while (1) {
		if (CHECK_BIT(PINF, PF5)) {
			PWM_ON(PORTB, PB2, 0.01);
		} else {
			SET_BIT_TO(PORTB, PB2, 0);
		}
	}
	return 0;
}