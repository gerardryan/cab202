#include "avr\io.h"
#include "util\delay.h"

#define SET_BIT_TO(reg, pin, val) (reg ^= ((val) ? ~reg : reg) & 1 << pin)
#define CHECK_BIT(reg, pin) (reg >> pin & 1)
#define TOGGLE_BIT(reg, pin) (reg ^= 1 << pin)

int main() {

	// setup switch inputs
	SET_BIT_TO(DDRD, PD1, 0); // UP
	SET_BIT_TO(DDRB, PB1, 0); // LEFT
	SET_BIT_TO(DDRB, PB7, 0); // DOWN
	SET_BIT_TO(DDRD, PD0, 0); // RIGHT
	SET_BIT_TO(DDRB, PB0, 0); // CENTER

	// setup LED outputs
	SET_BIT_TO(DDRB, PB2, 1); // LEFT
	SET_BIT_TO(DDRB, PB3, 1); // RIGHT

	while (1) {
		if (CHECK_BIT(PINB, PB0)) { // CENTER
			TOGGLE_BIT(PORTB, PB2); // LEFT
			TOGGLE_BIT(PORTB, PB3); // RIGHT
			_delay_ms(500);
		} else if (CHECK_BIT(PIND, PD1)) { // UP
			SET_BIT_TO(PORTB, PB2, 1); // LEFT
			SET_BIT_TO(PORTB, PB3, 1); // RIGHT
		} else if (CHECK_BIT(PINB, PB1)) { // LEFT
			SET_BIT_TO(PORTB, PB2, 1); // LEFT
			SET_BIT_TO(PORTB, PB3, 0); // RIGHT
		} else if (CHECK_BIT(PINB, PB7)) { // DOWN
			SET_BIT_TO(PORTB, PB2, 0); // LEFT
			SET_BIT_TO(PORTB, PB3, 0); // RIGHT
		} else if (CHECK_BIT(PIND, PD0)) { // RIGHT
			SET_BIT_TO(PORTB, PB2, 0); // LEFT
			SET_BIT_TO(PORTB, PB3, 1); // RIGHT
		}
	}

	return 0;
}