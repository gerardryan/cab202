#include "avr\io.h"
#include "cpu_speed.h"
#include "util\delay.h"

#include "led_pwm.h"

#define SET_BIT_TO(reg, pin, val) (reg ^= ((val) ? ~reg : reg) & 1 << pin)
#define TOGGLE_BIT(reg, pin) (reg ^= 1 << pin)

int main() {
	set_clock_speed(CPU_8MHz);

	// Teensy LED is connected to B2, configure pin 2 as an output(1)
	SET_BIT_TO(DDRB, PB2, 1);

	// turn OFF LED initially
	SET_BIT_TO(PORTB, PB2, 0);

	unsigned char ledOn = 0;
	unsigned short msCount = 0;
	while (1) {

		// on with PWM or off for 1ms
		if (ledOn) {
			PWM_ON(PORTB, PB2, 0.01);
		} else {
			_delay_ms(1);
		}

		// toggle led every 500ms
		if (++msCount >= 500) {
			ledOn ^= 0xff;
			msCount = 0;
		}
	}

	return 0;
}