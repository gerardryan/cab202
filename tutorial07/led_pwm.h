#include "avr\io.h"
#include "util\delay.h"

#define PWM_PEROID 1000 // us
#define PWM_ON(port, pin, width) (port |= 1 << pin, _delay_us(width * PWM_PEROID), port &= ~(1 << pin), _delay_us((1 - width) * PWM_PEROID))