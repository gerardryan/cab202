/*  CAB202 Tutorial 9
*	Question 4 - Solution
*
*	B.Talbot, May 2016
*	Queensland University of Technology
*/
#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/interrupt.h>

#include "lcd.h"
#include "graphics.h"
#include "cpu_speed.h"
#include "sprite.h"

/*
* Useful defines you can use in your system time calculations
*/
#define FREQUENCY 8000000.0
#define PRESCALER 1024.0

#define SET_BIT_TO(reg, pin, val) (reg ^= ((val) ? ~reg : reg) & 1 << pin)
#define CHECK_BIT(reg, pin) (reg >> pin & 1)
#define TOGGLE_BIT(reg, pin) (reg ^= 1 << pin)

#define I 0
#define O 1

#define LSB 0
#define MSB 1

/*
* Definitions for the states of the buttons
*/
#define NUM_BUTTONS 6
#define BTN_DPAD_LEFT 0
#define BTN_DPAD_RIGHT 1
#define BTN_DPAD_UP 2
#define BTN_DPAD_DOWN 3
#define BTN_LEFT 4
#define BTN_RIGHT 5

#define BTN_STATE_UP 0
#define BTN_STATE_DOWN 1

/*
* Variables used in recording the state of the buttons. Note the use of volatile
* keyword. For arrays this keyword isn't necessary (consider why...), but we leave
* it there to show how to use volatile Sprite variables with the "cab202_teensy"
* library
*/
volatile Sprite btn_sprites[NUM_BUTTONS];
volatile unsigned char btn_hists[NUM_BUTTONS];
volatile unsigned char btn_states[NUM_BUTTONS];

volatile unsigned int press_count = 0;

/*
* Bitmaps for visualising the button states
*/
unsigned char bm_pressed[] = {
    0b11111111,
    0b11111111,
    0b11111111,
    0b11111111,
    0b11111111,
    0b11111111,
    0b11111111,
    0b11111111
};
unsigned char bm_unpressed[] = {
    0b11111111,
    0b10000001,
    0b10000001,
    0b10000001,
    0b10000001,
    0b10000001,
    0b10000001,
    0b11111111
};

/*
* Function declarations
*/
void init_hardware(void);

double get_system_time(unsigned int timer_count);

/*
* Main - initialise the hardware, and run the code that increments the face's
* current position and draws it.
*/
int main() {
    set_clock_speed(CPU_8MHz);

    // Create and initialise the sprites
    // NOTE: our volatile Sprite pointers are explicitly cast back to a Sprite
    // pointer to remove the "volatile" qualifier. This is because our functions
    // in the library do not explicitly support volatile arguments!
    init_sprite((Sprite*) &btn_sprites[BTN_DPAD_LEFT], 0, 32, 8, 8, bm_unpressed);
    init_sprite((Sprite*) &btn_sprites[BTN_DPAD_RIGHT], 16, 32, 8, 8, bm_unpressed);
    init_sprite((Sprite*) &btn_sprites[BTN_DPAD_UP], 8, 24, 8, 8, bm_unpressed);
    init_sprite((Sprite*) &btn_sprites[BTN_DPAD_DOWN], 8, 40, 8, 8, bm_unpressed);
    init_sprite((Sprite*) &btn_sprites[BTN_LEFT], 60, 32, 8, 8, bm_unpressed);
    init_sprite((Sprite*) &btn_sprites[BTN_RIGHT], 76, 32, 8, 8, bm_unpressed);

    // Setup the hardware
    init_hardware();

    // Run the main loop
    char buff[80];
    while (1) {
        // Decide what bitmap we should have
        for (unsigned char i = 0; i< NUM_BUTTONS; i++) {
            btn_sprites[i].bitmap = (btn_states[i] == BTN_STATE_DOWN) ? bm_pressed : bm_unpressed;
        }

        // Draw the screen
        clear_screen();
        for (unsigned char i = 0; i < NUM_BUTTONS; i++) {
            // NOTE: the same casting technique is used as mentioned above.
            draw_sprite((Sprite*) &btn_sprites[i]);
        }
        sprintf(buff, "%5.4f", get_system_time(TCNT1));
        draw_string(54, 0, buff);
        sprintf(buff, "%4d", press_count);
        draw_string(0, 0, buff);
        show_screen();

        // Have a rest
        _delay_ms(50);
    }

    // We'll never get here...
    return 0;
}

/*
* Function implementations
*/
void init_hardware(void) {
    // Initialising the LCD screen
    lcd_init(LCD_DEFAULT_CONTRAST);

	// setup switch inputs
	SET_BIT_TO(DDRD, PD1, I); // UP
	SET_BIT_TO(DDRB, PB1, I); // LEFT
	SET_BIT_TO(DDRB, PB7, I); // DOWN
	SET_BIT_TO(DDRD, PD0, I); // RIGHT

	// Initalising the buttons as inputs
	SET_BIT_TO(DDRF, PF6, I); // LEFT 
	SET_BIT_TO(DDRF, PF5, I); // RIGHT

	// Initialising the LEDs as outputs
	SET_BIT_TO(DDRB, PB2, O); // LEFT
	SET_BIT_TO(DDRB, PB3, O); // RIGHT

	// Set the pins to GUARANTEE that TIMER1 is in "normal" operation mode
	TCCR1B &= ~((1 << WGM13) | (1 << WGM12));

	// Set the prescaler for TIMER1 so that the clock overflows every ~8.3 seconds
	TCCR1B |= (1 << CS12) | (1 << CS10); TCCR1B &= ~((1 << CS11));

	// Set the pins to GUARANTEE that TIMER0 is in "normal" operation mode
	TCCR0B &= ~(1 << WGM02);

	// Set the prescaler for TIMER0 so that the clock overflows every ~2 miliseconds
	TCCR0B |= (1 << CS02); TCCR1B &= ~((1 << CS01) | (1 << CS00));

	// Enable the Timer Overflow Interrupt for TIMER0
	TIMSK0 |= (1 << TOIE0);

    // Globally enable interrupts
	sei();
}

double get_system_time(unsigned int timer_count) {
	// Based on the current count, frequency, and prescaler - return the current
	// count time in seconds
	return (1.0 / (FREQUENCY / PRESCALER) * (double)timer_count);
}

/*
* Interrupt service routines
*/
ISR(TIMER0_OVF_vect) {
	for (unsigned char i = 0; i < NUM_BUTTONS; i++)
		btn_hists[i] <<= 1;

	// store switch state
	SET_BIT_TO(btn_hists[BTN_DPAD_UP], LSB, CHECK_BIT(PIND, PD1)); // UP
	SET_BIT_TO(btn_hists[BTN_DPAD_LEFT], LSB, CHECK_BIT(PINB, PB1)); // LEFT
	SET_BIT_TO(btn_hists[BTN_DPAD_DOWN], LSB, CHECK_BIT(PINB, PB7)); // DOWN
	SET_BIT_TO(btn_hists[BTN_DPAD_RIGHT], LSB, CHECK_BIT(PIND, PD0)); // RIGHT

	// store buttons states
	SET_BIT_TO(btn_hists[BTN_LEFT], LSB, CHECK_BIT(PINF, PF6)); // LEFT 
	SET_BIT_TO(btn_hists[BTN_RIGHT], LSB, CHECK_BIT(PINF, PF5)); // RIGHT

	for (unsigned char i = 0; i < NUM_BUTTONS; i++) {
		if (btn_states[i]) { // button state down
			if (btn_hists[i] == 0x00) { // button history is up
				btn_states[i] = BTN_STATE_UP;
				press_count++;
			}
		} else { // button state up
			if (btn_hists[i] == 0xff) { // button history is down 
				btn_states[i] = BTN_STATE_DOWN;
			}
		}
	}
}
